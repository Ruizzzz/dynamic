#include "detection.h"
#include "image.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

//extern void test_detector(char *datacfg, char *cfgfile, char *weightfile, char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen);

typedef unsigned char byte;
// exec ./detection /home/spark/tools/yolo/yolov3.cfg /home/spark/tools/yolo/yolov3.weights -i 0 -fullscreen 1 -out ./1_after.png
int main(int argc, char **argv)
{
    //test_resize("data/bad.jpg");
    //test_box();
    //test_convolutional_layer();
    // 这里需要两个参数，配置文件和权重文件
    if (argc < 2)
    {
        fprintf(stderr, "usage: %s <function>\n", argv[0]);
        return 0;
    }
    // 选择gpuid 默认是0
    gpu_index = find_int_arg(argc, argv, "-i", 0);
    if (find_arg(argc, argv, "-nogpu"))
    {
        gpu_index = -1;
    }
    // 如果没有定义GPU，则不s使用GPU
#ifndef GPU
    gpu_index = -1;
#else
    if (gpu_index >= 0)
    {
        cuda_set_device(gpu_index);
    }
#endif

    float thresh = find_float_arg(argc, argv, "-thresh", .5);
    //    char *filename = (argc > 3) ? argv[3]: 0;
    char *outfile = find_char_arg(argc, argv, "-out", 0);
    int fullscreen = find_arg(argc, argv, "-fullscreen");

    char **names = load_names("/home/spark/tools/yolo/coco.data");
    // argv[1] = "", argv[2] = ""
    network *net = load_network_test(argv[1], argv[2], 1, gpu_index, 0);
    //    printf("%s\n",names[0]);
    //   load_network_test(argv[1], argv[2],net);
    float hier_thresh = 0.5;

    int w = 1021;
    int h = 579;
    int c = 3;

    // //file
    // FILE *fp = NULL;
    // fp = fopen("/home/spark/tools/yolo/test.jpg", "r");
    // if (!fp)
    //     exit(0);
    unsigned char *data = jpg2BytesInC("/home/spark/tools/demo_data/2_b.png",3);

    //unsigned char* data =(unsigned char*)malloc(sizeof(unsigned char)*w*h*c);

    // for (int i = 0; i < w * h * c; i++)
    // {
    //     fscanf(fp, "%e", (data + i));
    // }
    // fclose(fp);

    /*   for (int i=0;i<w*h*c;i++){
	printf("%f\n",data[i]);
    }
*/
    // boxesAndAcc* bTest =  detectByInputBytes(_bytes, thresh, 0.5, _outfile, fullscreen, names, net, ww, hh, c, labelpaths);
    char *labelpath = "/home/spark/tools/yolo/labels";
    char *datacfg = "/root/map123/mlr/darknet/cfg/imagenet1k.data";
    char *bTest = detectByInputBytes(data, thresh, hier_thresh, outfile, fullscreen, names, net, w, h, c, labelpath,datacfg,5);
    //    boxesAndAcc* bTest = detect_box_and_acc( filename,data, thresh, hier_thresh, outfile, fullscreen,names,net,w,h,c);
    //    boxesAndAcc* bTest = test_detect_box_and_acc_1("cfg/coco.data", argv[1], argv[2], filename, thresh, 0.5, outfile, fullscreen,net);
    //    boxesAndAcc* bTest = test_detect_box_and_acc("cfg/coco.data", argv[1], argv[2], filename, thresh, .5, outfile, fullscreen,net);
    
//    if (bTest != NULL)
//    {
//        int size = bTest[0].size;
//        printf("size= %d L\n", size);
//        for (int i = 0; i < size; i++)
//        {
//            if (bTest[i].isVaild)
//            {
//                printf("name:%s:acc:%f\n", bTest[i].names, bTest[i].acc);
//                box boxInstance = bTest[i].boxes;
//                ;
//                printf("x:%f,y:%f,w:%f,h:%f\n", boxInstance.x, boxInstance.y, boxInstance.w, boxInstance.h);
//            }
//        }
//        free(bTest);
//    }
//    else
//    {
//        printf("bTest is NULL\n");
//    }
//    free(data);
    return 0;
}
