#include "detection.h"
#include "cuda.h"
#include <sys/timeb.h>
char **load_names(char *datacfg)
{
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", 0);
    if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
    char **names = get_labels(name_list);
    for(int sss=0;sss<10;sss++){
        	printf("%s",names[sss]);
        	}
        return names;
}
extern long getCurrentTime()
{
    struct timeb t;
    ftime(&t);
    return t.time * 1000 + t.millitm;
}

/**
 * 遍历神经网络，外层指针线程独享，layer层内的float线程共享
*/
network *sharing_network(network *firstPeer, int gpuindex) {
    printf("ggggggggggggggggggggg9999");
    if(gpuindex >= 0){
        cuda_set_device(gpuindex);
    }
    network *mynetPtr = (network*) calloc(1, sizeof(network));
    *mynetPtr = *firstPeer;
    // printf("赋值成功 指针 = %p\n", mynetPtr);
    // make_network
    mynetPtr->layers = (layer*)calloc(mynetPtr->n, sizeof(layer));
    // mynetPtr->seen = (size_t*)calloc(1, sizeof(size_t));
    // mynetPtr->t    = (int*)calloc(1, sizeof(int));
    mynetPtr->cost = (float*)calloc(1, sizeof(float)); // 会计算代价
    // printf("layer新申请空间\n");
#ifdef GPU
    // printf("secondGPU workspace\n");
    mynetPtr->workspace = cuda_make_array(0, (mynetPtr->workspace_size-1)/sizeof(float)+1);
#else
    mynetPtr->workspace = (float*)calloc(1, mynetPtr->workspace_size);
#endif
    // make_network
    // 遍历每一层
    // printf("for before\n");
    for (size_t i = 0; i < mynetPtr->n; i++)
    {
        // 浅拷贝
        mynetPtr->layers[i] = firstPeer->layers[i];
        // backward 才会使用？
        if (mynetPtr->layers[i].type == CONVOLUTIONAL)
        {
            // printf("layer type = CONVOLUTIONAL\n");
            // 浅拷贝
            // 内存部分
            //l.output =(float*) calloc(l.batch*l.outputs, sizeof(float));
            mynetPtr->layers[i].output = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
            // backward才会使用？
            // mynetPtr->layers[i].delta  = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
            // 显存部分
             
        #ifdef GPU
            // mynetPtr->layers[i].delta_gpu = cuda_make_array(mynetPtr->layers[i].delta,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            // printf("for GPU before %d \n", i);
            //l.output_gpu = cuda_make_array(l.output, l.batch*out_h*out_w*n);       
            printf("mynetPtr->layers[i].output = %p,  mynetPtr->layers[i].batch = %d, mynetPtr->layers[i].out_h = %d,mynetPtr->layers[i].out_w = %d, mynetPtr->layers[i].n = %d\n",mynetPtr->layers[i].output,mynetPtr->layers[i].batch,mynetPtr->layers[i].out_h,mynetPtr->layers[i].out_w,firstPeer->layers[i].n );   
            mynetPtr->layers[i].output_gpu = cuda_make_array(mynetPtr->layers[i].output,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            printf("for GPU after %d \n", i);
        #endif
        #ifdef CUDNN
            // 暂时不需要 
        #endif
        } else if (mynetPtr->layers[i].type == ROUTE)
        {
            printf("layer type = ROUTE\n");
            // 内存部分
            mynetPtr->layers[i].output = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
            #ifdef GPU
            // mynetPtr->layers[i].delta_gpu = cuda_make_array(mynetPtr->layers[i].delta,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            // l.output_gpu = cuda_make_array(l.output, outputs*batch);
            mynetPtr->layers[i].output_gpu = cuda_make_array(mynetPtr->layers[i].output,  mynetPtr->layers[i].outputs*mynetPtr->layers[i].batch);
            #endif
        }else if (mynetPtr->layers[i].type == SHORTCUT)
        {
            printf("layer type = SHORTCUT\n");
            // 内存部分
            mynetPtr->layers[i].output = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
        #ifdef GPU
            // mynetPtr->layers[i].delta_gpu = cuda_make_array(mynetPtr->layers[i].delta,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            printf("for GPU before %d \n", i);
            // l.output_gpu = cuda_make_array(l.output, outputs*batch);
            mynetPtr->layers[i].output_gpu = cuda_make_array(mynetPtr->layers[i].output,  mynetPtr->layers[i].outputs*mynetPtr->layers[i].batch);
        #endif
        }else if (mynetPtr->layers[i].type == YOLO)
        {
            printf("layer type = YOLO\n");

            // 内存部分
            mynetPtr->layers[i].output = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
        #ifdef GPU
            // mynetPtr->layers[i].delta_gpu = cuda_make_array(mynetPtr->layers[i].delta,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            printf("for GPU before %d \n", i);
            // l.output_gpu = cuda_make_array(l.output, outputs*batch);
            mynetPtr->layers[i].output_gpu = cuda_make_array(mynetPtr->layers[i].output,  mynetPtr->layers[i].outputs*mynetPtr->layers[i].batch);
        #endif
        }else if (mynetPtr->layers[i].type == UPSAMPLE)
        {
            printf("layer type = UPSAMPLE\n");
            // 内存部分
            mynetPtr->layers[i].output = (float*) calloc(mynetPtr->layers[i].batch*mynetPtr->layers[i].outputs, sizeof(float));
        #ifdef GPU
            // mynetPtr->layers[i].delta_gpu = cuda_make_array(mynetPtr->layers[i].delta,  mynetPtr->layers[i].batch*mynetPtr->layers[i].out_h*mynetPtr->layers[i].out_w*mynetPtr->layers[i].n);
            printf("for GPU before %d \n", i);
            // l.output_gpu = cuda_make_array(l.output, outputs*batch);
            mynetPtr->layers[i].output_gpu = cuda_make_array(mynetPtr->layers[i].output,  mynetPtr->layers[i].outputs*mynetPtr->layers[i].batch);
        #endif
        }
        else
        {
            printf("暂不支持此类型网络");
        }
    }
    printf("layer 二次改造完成\n");

// 输出层
    layer out = get_network_output_layer(mynetPtr);
    
    mynetPtr->outputs = out.outputs;
    mynetPtr->truths = out.outputs;
    if(mynetPtr->layers[mynetPtr->n-1].truths) mynetPtr->truths = mynetPtr->layers[mynetPtr->n-1].truths;
    mynetPtr->output = out.output;
    // 似乎是多余的
    mynetPtr->input = (float*)calloc(mynetPtr->inputs*mynetPtr->batch, sizeof(float));
    mynetPtr->truth = (float*)calloc(mynetPtr->truths*mynetPtr->batch, sizeof(float));
#ifdef GPU
    // 重新赋值，这里存在问题 应该有 gpu_index >=0 的判断
    mynetPtr->output_gpu = out.output_gpu;
    mynetPtr->input_gpu = cuda_make_array(mynetPtr->input, mynetPtr->inputs*mynetPtr->batch);
    mynetPtr->truth_gpu = cuda_make_array(mynetPtr->truth, mynetPtr->truths*mynetPtr->batch);
#endif
    printf("结束\n");
    return mynetPtr;
}
network *load_network_test_sharing(char *cfgfile, char *weightfile, int batchsize, int gpu_index_params, char *gpuid)
{

    //printf("from java batchsize: %d\n", batchsize);
    //printf("from java gpuids: %s\n", gpuid);

    long load_networkStart = getCurrentTime();
// #ifdef GPU
    printf("ggggggggggggggggggggg0");
    if(gpu_index_params >= 0){
        cuda_set_device(gpu_index_params);
    }
// #endif
    network *net = load_network(cfgfile, weightfile, 0, gpu_index_params);
    long load_networkEnd = getCurrentTime();
    printf("load time = %ld \n", load_networkEnd - load_networkStart);
    set_batch_network(net, 1);
    //
    //set_batch_network(net, batchsize);
    //printf("actual net batchsize: %d\n", net->batch);

    return net;
}



network *load_network_test(char *cfgfile, char *weightfile, int batchsize, int gpu_index_params, char *gpuid)
{

    //printf("from java batchsize: %d\n", batchsize);
    //printf("from java gpuids: %s\n", gpuid);

    long load_networkStart = getCurrentTime();
    if(gpu_index_params >= 0){
        cuda_set_device(gpu_index_params);
    }
    network *net = load_network(cfgfile, weightfile, 0, gpu_index_params);
// #ifdef GPU
    printf("ggggggggggggggggggggg1");
 
// #endif
    long load_networkEnd = getCurrentTime();
    printf("load time = %ld \n", load_networkEnd - load_networkStart);
    set_batch_network(net, 1);
    //
    //set_batch_network(net, batchsize);
    //printf("actual net batchsize: %d\n", net->batch);

    return net;
}

//#define INT_MAX 2147483647 //[64位编译器int最大值]
#define E_MAX 255

//int *png_size(FILE *fp1)
//{                //得到PNG文件中二进制数据输出宽高（单位：像素）
//    rewind(fp1); //文件重定向到开始
//    int place = 0, index = 0, i;
//    char data;
//    int size[8];
//    int width = 0, height = 0;
//    while (place <= 24)
//    {
//        place++;
//        data = fgetc(fp1);
//        if (place > 16)
//        {
//            size[index] = (int)data;
//            index++;
//        }
//    }
//    for (i = 0; i < 4; i++)
//        if (size[i] < 0)
//            size[i] = (size[i] & INT_MAX) & E_MAX;
//    width = 256 * size[2] + size[3];
//    height = 256 * size[6] + size[7];
//    printf("【尺寸】图片宽为%d，长为%d\n", width, height);
//    // stati int size_w_h[2] = {0, 0};
//    int size_w_h[2] = {0, 0};
//    size_w_h[0] = width;
//    // size_w_h[0] = height;
//    size_w_h[1] = height;
//    return size_w_h;
//}

boxesAndAcc *detect_box_and_acc(char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen, char **names, network *net, int w, int h, int c) //  now...
{

    boxesAndAcc *boxesAndAccInstall;
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    float nms = .45;

    image **alphabet = load_alphabet();

    while (1)
    {
        if (filename)
        {
            strncpy(input, filename, 256);
        }
        else
        {
            printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if (!input)
                return NULL;
            strtok(input, "\n");
        }
        /*
	// 0000
        image im = load_image_color(input,0,0);
        mage sized = letterbox_image(im, net->w, net->h);
*/
        //0001
        //int * size_wh = png_size(fopen(input, "rb+"));
        //int width = size_wh[0];
        //int height = size_wh[1];

        unsigned char *bytes = matToBytes(input, c);

        image im = bytesToMat2image(bytes, w, h);

        image sized = letterbox_image(im, net->w, net->h);
        //printf("height: %d.\n", h);

        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n - 1];

        float *X = sized.data;
        // time = what_time_is_it_now();
        network_predict(net, X);
        // printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now() - time);
        int nboxes = 0;
        // 获取结果
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        //printf("%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms)
            do_nms_sort(dets, nboxes, l.classes, nms);

        boxesAndAccInstall = (boxesAndAcc*)malloc(sizeof(boxesAndAcc) * nboxes);
        int *validBox = (int*)malloc(sizeof(int) * nboxes);

        for (int i = 0; i < nboxes; ++i)
        {
            validBox[i] = 0;
            boxesAndAccInstall[i].isVaild = false;
            char labelstr[4096] = {0};
            int class = -1;
            float acc = 0.0;
            for (int j = 0; j < l.classes; ++j)
            {

                if (dets[i].prob[j] > thresh)
                {
                    validBox[i]++;
                    if (class < 0)
                    {
                        strcat(labelstr, names[j]);
                        class = j;
                    }
                    else
                    {
                        strcat(labelstr, ", ");
                        strcat(labelstr, names[j]);
                    }
                    //	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
                    boxesAndAccInstall[i].isVaild = true;
                    if (acc < dets[i].prob[j] * 100)
                    {
                        acc = dets[i].prob[j] * 100;
                        boxesAndAccInstall[i].acc = acc;
                        boxesAndAccInstall[i].names = names[j];
                    }
                }
            }
            if (validBox[i] > 0)
            {
                boxesAndAccInstall[i].boxes = dets[i].bbox;
                boxesAndAccInstall[i].size = nboxes;
            }
        }

        free(validBox);
        //	printf("%d\n", nboxes);
        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
        free_detections(dets, nboxes);
        //	printf("%d\n", nboxes);
        if (outfile)
        {
            save_image(im, outfile);
        }
        else
        {
            save_image(im, "predictions");
#ifdef OPENCV
            make_window("predictions", 512, 512, 0);
            show_image(im, "predictions", 0);
#endif
        }

        free_image(im);
        free_image(sized);
        if (filename)
            break;
    }
    return boxesAndAccInstall;
}

boxesAndAcc *test_detect_box_and_acc(char *datacfg, char *cfgfile, char *weightfile, char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen)
{
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);
    boxesAndAcc *boxesAndAccInstall;
    //    image **alphabet = load_alphabet();
    network *net = load_network(cfgfile, weightfile, 0, 0);
    set_batch_network(net, 1);
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    float nms = .45;

    while (1)
    {
        if (filename)
        {
            strncpy(input, filename, 256);
        }
        else
        {
            printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if (!input)
                return NULL;
            strtok(input, "\n");
        }
        image im = load_image_color(input, 0, 0);
        image sized = letterbox_image(im, net->w, net->h);
        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n - 1];

        float *X = sized.data;
        time = what_time_is_it_now();
        network_predict(net, X);
        printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now() - time);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        //printf("%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms)
            do_nms_sort(dets, nboxes, l.classes, nms);

        boxesAndAccInstall = (boxesAndAcc*)malloc(sizeof(boxesAndAcc) * nboxes);
        int *validBox = (int*)malloc(sizeof(int) * nboxes);

        for (int i = 0; i < nboxes; ++i)
        {
            validBox[i] = 0;
            boxesAndAccInstall[i].isVaild = false;
            char labelstr[4096] = {0};
            int class = -1;
            float acc = 0.0;
            for (int j = 0; j < l.classes; ++j)
            {

                if (dets[i].prob[j] > thresh)
                {
                    validBox[i]++;
                    if (class < 0)
                    {
                        strcat(labelstr, names[j]);
                        class = j;
                    }
                    else
                    {
                        strcat(labelstr, ", ");
                        strcat(labelstr, names[j]);
                    }
                    //	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
                    boxesAndAccInstall[i].isVaild = true;
                    if (acc < dets[i].prob[j] * 100)
                    {
                        acc = dets[i].prob[j] * 100;
                        boxesAndAccInstall[i].acc = acc;
                        boxesAndAccInstall[i].names = names[j];
                    }
                }
            }
            if (validBox[i] > 0)
            {
                boxesAndAccInstall[i].boxes = dets[i].bbox;
                boxesAndAccInstall[i].size = nboxes;
            }
        }

        free(validBox);
        //	printf("%d\n", nboxes);
        //        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
        //        free_detections(dets, nboxes);
        //	printf("%d\n", nboxes);
        /*       if(outfile){
            save_image(im, outfile);
        }
        else{
            save_image(im, "predictions");
#ifdef OPENCV
            make_window("predictions", 512, 512, 0);
            show_image(im, "predictions", 0);
#endif
        }*/

        free_image(im);
        free_image(sized);
        if (filename)
            break;
    }
    return boxesAndAccInstall;
}

boxesAndAcc *detectByInputBuffer(float *buffer, float thresh, float hier_thresh, char *outfile, int fullscreen, char **names, network *net, int w, int h, int c)
{
    //CPU for default, gpu_index = 1 for GPU.

    //printf("GPU index: %d....\n", gpu_index);

    //if(gpu_index>=0){
    //   	cuda_set_device(gpu_index);
    //}
#ifndef GPU
    gpu_index = -1;
#else
    if (gpu_index >= 0)
    {
        cuda_set_device(gpu_index);
    }
#endif
    //net->gpu_index = gpu_index;
    boxesAndAcc *boxesAndAccInstall;
    srand(2222222);
    double time;
    //   char buff[256];
    //    char *input = buff;

    //printf("sizeof buffer: %ld\n", sizeof(buffer));
    //printf("buffer 7? %lf\n", buffer[7]);
    //printf("buffer 8? %lf\n", buffer[8]);
    //printf("buffer 9? %lf\n", buffer[9]);

    image **alphabet = load_alphabet();

    float nms = .45;
    if (1)
    {

        //        image im = load_image_color(input,0,0);
        //printf("buffer:%f\n",buffer[0]);
        image im = load_float_and_whc(buffer, w, h, c);
        //printf("img:%f\n",im.data[0]);
        image sized = letterbox_image(im, net->w, net->h);
        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n - 1];

        float *X = sized.data;
        /*	for(int i=0;i<608*608*3;i++){
		printf("%f\n",X[i],i);
	}
*/
        printf("w:%d,h:%d,c:%d\n", im.w, im.h, im.c);
        time = what_time_is_it_now();

        network_predict(net, X);

        printf("Predicted in %f seconds.\n", what_time_is_it_now() - time);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        //        printf("boxes:%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms)
            do_nms_sort(dets, nboxes, l.classes, nms);

        boxesAndAccInstall = (boxesAndAcc*)malloc(sizeof(boxesAndAcc) * nboxes);
        int *validBox = (int*)malloc(sizeof(int) * nboxes);

        for (int i = 0; i < nboxes; ++i)
        {
            validBox[i] = 0;
            boxesAndAccInstall[i].isVaild = false;
            char labelstr[4096] = {0};
            int class = -1;
            float acc = 0.0;
            for (int j = 0; j < l.classes; ++j)
            {

                if (dets[i].prob[j] > thresh)
                {
                    validBox[i]++;
                    if (class < 0)
                    {
                        strcat(labelstr, names[j]);
                        class = j;
                    }
                    else
                    {
                        strcat(labelstr, ", ");
                        strcat(labelstr, names[j]);
                    }
                    //	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
                    boxesAndAccInstall[i].isVaild = true;
                    if (acc < dets[i].prob[j] * 100)
                    {
                        acc = dets[i].prob[j] * 100;
                        boxesAndAccInstall[i].acc = acc;
                        boxesAndAccInstall[i].names = names[j];
                    }
                }
            }
            if (validBox[i] > 0)
            {
                boxesAndAccInstall[i].boxes = dets[i].bbox;
                boxesAndAccInstall[i].size = nboxes;
            }
        }

        free(validBox);
        //	printf("%d\n", nboxes);
        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
        free_detections(dets, nboxes);
        //	printf("%d\n", nboxes);
        if (outfile)
        {
            save_image(im, outfile);
        }
        else
        {
            save_image(im, "predictions");
#ifdef OPENCV
            printf("I am OPENCV...\n");
            // make_window("predictions", 512, 512, 0);
            // show_image(im, "predictions", 0);
#endif
        }

        free_image(im);
        free_image(sized);
    }
    return boxesAndAccInstall;
}

//   jpg to bytes...
unsigned char *jpg2BytesInC(char *filename, int channels)
{

    unsigned char *bytes = matToBytes(filename, channels);
    //printf("InC, bytes[1]: %d\n", bytes[1]);
    return bytes;
}

// detect by bytes...
char *detectByInputBytes(unsigned char *bytes, float thresh, float hier_thresh, char *outfile, int fullscreen, char **names, network *net, int w, int h, int c, char *labelpath,char *datacfg,int top) //  now...
{

    //printf("****************************detectByInputBytes in cpp.\n");
    boxesAndAcc *boxesAndAccInstall;
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    // nmsThresh
    float nms = .45;

    image **alphabet = load_alphabet(labelpath);
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", 0);
    if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
    if(top == 0) top = option_find_int(options, "top", 1);
    int *indexes = calloc(top, sizeof(int));
    char **namess = get_labels(name_list);
    while (1)
    {
        // 字节转image
        image im = bytesToMat2image(bytes, w, h);
        // 标准化后的图片
        // printf("第一次读取net的内容 %p", net);
        image sized = letterbox_image(im, net->w, net->h);
        // printf("第一次读取net的内容 net->w = %d, net->h = %d",net->w, net->h);
        // 最后一层
//        layer l = net->layers[net->n - 1];
        // 标准化的图片数据
        float *X = sized.data;
        // 获得时间戳
        // time = what_time_is_it_now();
        // 使用GPU或CPU预测
        time=what_time_is_it_now();
        float *predictions = network_predict(net, X);
        if(net->hierarchy) hierarchy_predictions(predictions, net->outputs, net->hierarchy, 1, 1);
        top_k(predictions, net->outputs, top, indexes);
        printf("Predicted in %f seconds.\n", what_time_is_it_now()-time);
        int index = indexes[0];
        printf("%5.2f%%: %s\n", predictions[index]*100, namess[index]);
        if(sized.data != im.data) free_image(sized);
                        free_image(im);
        return namess[index];
        }
        // printf("%s: Predicted in %f second.\n", input, what_time_is_it_now() - time);
        // printf("Predicted in %f second.\n", what_time_is_it_now() - time);
//        int nboxes = 0;
        // 获取预测之后的结果 nboxes = number of box = len = 异常
//        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // printf("\n !!!!!! nboxes = %d", nboxes);
//        if (nms)
//            do_nms_sort(dets, nboxes, l.classes, nms);
//        // 哪些box是有效的
//        boxesAndAccInstall = (boxesAndAcc*)malloc(sizeof(boxesAndAcc) * nboxes);
//        int *validBox = (int*)malloc(sizeof(int) * nboxes);
//        int i;
//        // 遍历每一个box i
//        for (i = 0; i < nboxes; ++i)
//        {
//            validBox[i] = 0;
//            boxesAndAccInstall[i].isVaild = false;
//            char labelstr[4096] = {0};
//            int class = -1;
//            float acc = 0.0;
//            // 计算每一个box与 物体j的距离是否大于阈值
//            for (int j = 0; j < l.classes; ++j)
//            {
//                // 如果大于阈值则此box是有效的
//                if (dets[i].prob[j] > thresh)
//                {
//                    validBox[i]++;
//                    // 如果还未分类 lable:cat
//                    if (class < 0)
//                    {
//                        strcat(labelstr, names[j]);
//                        class = j;
//                    }
//                    else // lable:cat , dog
//                    {
//                        strcat(labelstr, ", ");
//                        strcat(labelstr, names[j]);
//                    }
//                    //	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
//                    // 这个box是有效的
//                    boxesAndAccInstall[i].isVaild = true;
//                    // 选出概率最高的类别
//                    if (acc < dets[i].prob[j] * 100)
//                    {
//                        acc = dets[i].prob[j] * 100;
//                        boxesAndAccInstall[i].acc = acc;
//                        boxesAndAccInstall[i].names = names[j];
//                    }
//                }
//            }
//            // 如果第i个box是有效的，则拿到box的位置，这张图片box的数量
//            if (validBox[i] > 0)
//            {
//                // printf();
//                boxesAndAccInstall[i].boxes = dets[i].bbox;
//                boxesAndAccInstall[i].size = nboxes;
//            }
//        }
//
//        //printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$detectByInputBytes in cpp.\n");
//
//        free(validBox);
//        //	printf("%d\n", nboxes);
//        // 画图用于，保存操作(用于测试)  性能模式下，都要注释掉
//        // draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
//        free_detections(dets, nboxes);
//        // 保存操作
///*         //	printf("%d\n", nboxes);
//        if (outfile)
//        {
//            save_image(im, outfile);
//        }
//        else
//        {
//            save_image(im, "predictions");
//#ifdef OPENCV
//            make_window("predictions", 512, 512, 0);
//            show_image(im, "predictions", 0);
//#endif
//            make_window("predictions", 512, 512, 0);
//            show_image(im, "predictions", 0);
//        }
//        */
//        free_image(im);
//        free_image(sized);
//        if (outfile)
//            break;
//    }
//    return boxesAndAccInstall;
}
