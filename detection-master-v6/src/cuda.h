#ifndef CUDA_H
#define CUDA_H

#include "detection.h"

#ifdef GPU

void check_error(cudaError_t status);
cublasHandle_t blas_handle();
int *cuda_make_int_array(int *x, size_t n);
void cuda_random(float *x_gpu, size_t n);
float cuda_compare(float *x_gpu, float *x, size_t n, char *s);
dim3 cuda_gridsize(size_t n);
//void cuda_set_device(int n);


cudaStream_t get_thread_cuda_stream();
// 销毁当前线程的GPU stream
void destoryThreadStream();
// 带有时间戳的错误检测 
void check_error_extended(cudaError_t status, const char *file, int line, const char *date_time);
// #define CHECK_CUDA(X) check_error_extended(X, __FILE__ " : " __FUNCTION__, __LINE__,  __DATE__ " - " __TIME__ );


#ifdef CUDNN
cudnnHandle_t cudnn_handle();
cudnnStatus_t cudnn_SetStream(cudnnHandle_t handle, cudaStream_t streamId);
#endif

#endif
#endif
