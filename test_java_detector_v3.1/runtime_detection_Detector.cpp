#include <stdio.h>
//#include "src_main_java_operator_Detector.h"
#include "runtime_detection_Detector.h"
#include "detection.h"
#include <string>
#include <iostream>
#include <sys/time.h> 
#include<sys/timeb.h>

using namespace std;

//global, label\datacfg 20191017
char* _datacfg = NULL;
char** names = NULL;

char* labelpaths = NULL;
static __thread int a = 0;
long getCurrentTime()  
{  
    timeb t;
    ftime(&t);
    return t.time*1000+t.millitm; 
}

jstring stoJstring(JNIEnv* env, char* pat)//change type char* into string
{
    jclass strClass = env->FindClass("Ljava/lang/String;");
    jmethodID ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = env->NewByteArray(strlen(pat));
    env->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*)pat);
    jstring encoding = env->NewStringUTF("utf-8");
    return (jstring)env->NewObject(strClass, ctorID, bytes, encoding);
}


//jstring to char*
char* jstring2char(JNIEnv* env, jstring gpuid)
{
	  char* rtn = NULL;
	  jclass clsstring = env->FindClass("java/lang/String");
	  jstring strencode = env->NewStringUTF("utf-8");
	  jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	  jbyteArray barr= (jbyteArray)env->CallObjectMethod(gpuid, mid, strencode);
	  jsize alen = env->GetArrayLength(barr);
	  jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
	  if (alen > 0)
	  {
	    rtn = (char*)malloc(alen + 1);
	    memcpy(rtn, ba, alen);
	    rtn[alen] = 0;
	  }
	  env->ReleaseByteArrayElements(barr, ba, 0);
	  return rtn;
}

#ifndef CPU
JNIEXPORT jlong JNICALL Java_runtime_detection_Detector_initializeSecond// 3-1: initialize(), return (jlong)peer.
  (JNIEnv *env , jobject obg , jlong peer, jint gpuIndex){
	network* net = (network*)peer;
	printf("second\n");
    // if(gpuIndex >= 0){
	// 	printf("888888 gpu = %d ", gpu_index);
    //     cuda_set_device(gpuIndex);
    // }
	network* myPeer = sharing_network(net, gpuIndex);

	printf("secondover %p\n", myPeer);
	return (jlong) myPeer;
}

//JNIEXPORT jlong JNICALL Java_src_main_java_operator_Detector_initialize// 3-1: initialize(), return (jlong)peer.
JNIEXPORT jlong JNICALL Java_runtime_detection_Detector_initialize// 3-1: initialize(), return (jlong)peer.
  (JNIEnv *env , jobject obg , jstring cfgfile, jstring weightfile, jstring datacfg, jstring labelpath, jint batchsize , jint gpu_index , jstring gpuid){
		// a++;
        // printf("第%d次初始化\n", a);
		// 解析jstring 获取配置文件位置
        char* _cfgfile = (char*)(env)->GetStringUTFChars(  cfgfile, 0 );
		// 解析jstring 获取配置文件位置
        char* _weightfile = (char*)(env)->GetStringUTFChars( weightfile, 0 );
		// 解析jstring 获取GPU id
        char* gpuids =jstring2char(env, gpuid);
        cout<<"Gpu-Index"<<gpu_index<<endl;
	// cout <<"load network time :"<< endLoad - startLoad<< "ms"<<endl;
		long startLoad = getCurrentTime();
		// 加载神经网络 获得指针
		network* peer = load_network_test(_cfgfile, _weightfile , batchsize , gpu_index , gpuids);
		long endLoad = getCurrentTime();
		// printf("指针地址 = %p", peer);
		// 释放jstring
		(env)->ReleaseStringUTFChars(  cfgfile, _cfgfile );
		(env)->ReleaseStringUTFChars(  weightfile, _weightfile );

		// label\datacfg 20191017 获取标签
		_datacfg = (char*)(env)->GetStringUTFChars( datacfg, 0 );
		long startLoadName = getCurrentTime();
		// 获得标签对应的名字
//		names = load_names(_datacfg);
        list *options = read_data_cfg(_datacfg);
        char *name_list = option_find_str(options, "names", 0);
        if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
        names = get_labels(name_list);

		long endLoadName =getCurrentTime();
        // 获取标签地址
    	long startjstring2char = getCurrentTime();
   		labelpaths =jstring2char(env, labelpath);
		long endjstring2char = getCurrentTime();
		// cout <<"load network time :"<< endLoad - startLoad<< "ms"<<endl;
		// cout <<"load name time :"<< endLoadName - startLoadName<< "ms"<<endl;
		// cout <<"jstring2char time :"<< endjstring2char - startjstring2char<< "ms"<<endl;
		// 返回以jlong的形式指针地址
		// a++;
		pthread_t tid;
        tid = pthread_self();
        printf("thread = %u ------- 第%p次初始化完毕\n", tid, &a);
// #ifdef GPU
//     if(net->gpu_index >= 0){
//         cuda_set_device(net->gpu_index);
//     }
// #endif
		return (jlong)peer;
}




//JNIEXPORT jbyteArray JNICALL Java_src_main_java_operator_Detector_jpg2Bytes
JNIEXPORT jbyteArray JNICALL Java_runtime_detection_Detector_jpg2Bytes
  (JNIEnv *env, jobject obj, jstring path, jint w, jint h, jint c){

      //printf("************************************************ This is the 3rd GetStringUTFChars...\n");
      char* _path = (char*)(env)->GetStringUTFChars(  path, 0 );
      unsigned char * bytes = jpg2BytesInC(_path, c);
      //printf("Detector.cpp bytes[1] %d.\n", bytes[1]);

	//C++中的BYTE[]转jbyteArray
	//nOutSize是BYTE数组的长度  BYTE pData[]
         //int nOutSize = sizeof(bytes)/sizeof(unsigned char);
         int nOutSize = w * h * c;

	//printf("size of bytes: %ld.\n",sizeof(bytes));
       // printf("size of unsigned char: %ld.\n", sizeof(unsigned char));
	//printf("whc, nOutSize: %d.\n", nOutSize);

	jbyte *by = (jbyte*)bytes;
	jbyteArray jarray = (env)->NewByteArray(nOutSize);
	(env)->SetByteArrayRegion(jarray, 0, nOutSize, by);

      //printf("jarray[0] %c.\n", jarray[0]);
      return jarray;

}



//JNIEXPORT jobjectArray JNICALL Java_runtime_detection_Detector_computeBoxesAndAccByInputBytes
//  (JNIEnv *, jobject, jlong, jstring, jbyteArray, jstring, jfloat, jfloat, jint, jint, jint, jint);

//JNIEXPORT jobjectArray JNICALL Java_src_main_java_operator_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
JNIEXPORT jstring JNICALL Java_runtime_detection_Detector_computeBoxesAndAccByInputBytes
  (JNIEnv *env, jobject obj, jlong structWrapperPeer, jbyteArray bytes, jstring outfile,jfloat thresh,jfloat hier_thresh,
  jint fullscreen,jint ww, jint hh, jint c){

        //printf("computeBoxesAndAccByInputBytes in cpp.\n");
	// 将 64位 long 转化为指针变量 warpper + sizeof(network)
	network* net = (network*)structWrapperPeer;
	// 输出文件地址，会有中间结果写到磁盘
  	char* _outfile = (char*)(env)->GetStringUTFChars( outfile, 0 );
    set_batch_network(net, 1);
	jobjectArray infos = NULL;
	jsize len;
	int i;

    //JNI jbyteArray转char*
	unsigned char *_bytes = NULL;
	jbyte *bytess = (env)->GetByteArrayElements(bytes, 0);
    int chars_len = (env)->GetArrayLength(bytes);
	_bytes = new unsigned char[chars_len + 1];
	memset(_bytes,0,chars_len + 1);
	memcpy(_bytes, bytess, chars_len);
	_bytes[chars_len] = 0;

	// 检测，生成多个检测框
	char * result =  detectByInputBytes(_bytes, thresh, 0.5, _outfile, fullscreen, names, net, ww, hh, c, labelpaths,_datacfg,5);

    jstring info = stoJstring(env,result);
    (env)->ReleaseByteArrayElements(bytes,bytess,0);
    return info;
}
#endif


#ifdef CPU

JNIEXPORT jlong JNICALL Java_runtime_detection_DetectorCPU_initializeCPU// 3-1: initialize(), return (jlong)peer.
  (JNIEnv *env , jobject obg , jstring cfgfile, jstring weightfile, jstring datacfg, jstring labelpath, jint batchsize , jint gpu_index , jstring gpuid){

		// a++;
        // printf("第%d次初始化\n", a);
		// 解析jstring 获取配置文件位置
        char* _cfgfile = (char*)(env)->GetStringUTFChars(  cfgfile, 0 );
		// 解析jstring 获取配置文件位置
        char* _weightfile = (char*)(env)->GetStringUTFChars( weightfile, 0 );
		// 解析jstring 获取GPU id
        char* gpuids =jstring2char(env, gpuid);
	
		// cout <<"load network time :"<< endLoad - startLoad<< "ms"<<endl;
		// long startLoad = getCurrentTime();
		// 加载神经网络 获得指针
		network* peer = load_network_test(_cfgfile, _weightfile , batchsize , gpu_index , gpuids);
		// long endLoad = getCurrentTime();
		// printf("指针地址 = %p", peer);
		// 释放jstring
		(env)->ReleaseStringUTFChars(  cfgfile, _cfgfile );
		(env)->ReleaseStringUTFChars(  weightfile, _weightfile );

		// label\datacfg 20191017 获取标签
		_datacfg = (char*)(env)->GetStringUTFChars( datacfg, 0 );
		// long startLoadName = getCurrentTime();
		// 获得标签对应的名字
		names = load_names(_datacfg);
		// long endLoadName =getCurrentTime();
        // 获取标签地址
    	// long startjstring2char = getCurrentTime();
   		labelpaths =jstring2char(env, labelpath);
		// long endjstring2char = getCurrentTime();
		// cout <<"load network time :"<< endLoad - startLoad<< "ms"<<endl;
		// cout <<"load name time :"<< endLoadName - startLoadName<< "ms"<<endl;
		// cout <<"jstring2char time :"<< endjstring2char - startjstring2char<< "ms"<<endl;
		// 返回以jlong的形式指针地址
		// a++;
		// pthread_t tid;
        // // tid = pthread_self();
        // // printf("thread = %u ------- 第%p次初始化完毕\n", tid, &a);
		
		return (jlong)peer;
}

//JNIEXPORT jobjectArray JNICALL Java_src_main_java_operator_Detector_computeBoxesCPU// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
JNIEXPORT jstring JNICALL Java_runtime_detection_DetectorCPU_computeBoxesCPU// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
  (JNIEnv *env, jobject obj, jlong structWrapperPeer, jbyteArray bytes, jstring outfile,jfloat thresh,jfloat hier_thresh,
  jint fullscreen,jint ww, jint hh, jint c){

        //printf("computeBoxesAndAccByInputBytes in cpp.\n");
	// 将 64位 long 转化为指针变量 warpper + sizeof(network)
	network* net = (network*)structWrapperPeer;
	// 输出文件地址，会有中间结果写到磁盘
  	char* _outfile = (char*)(env)->GetStringUTFChars( outfile, 0 );
    set_batch_network(net, 1);
	jobjectArray infos = NULL;
	jsize len;
	int i;

    //JNI jbyteArray转char*
	unsigned char *_bytes = NULL;
	jbyte *bytess = (env)->GetByteArrayElements(bytes, 0);
    int chars_len = (env)->GetArrayLength(bytes);
	_bytes = new unsigned char[chars_len + 1];
	memset(_bytes,0,chars_len + 1);
	memcpy(_bytes, bytess, chars_len);
	_bytes[chars_len] = 0;

        char * result =  detectByInputBytes(_bytes, thresh, 0.5, _outfile, fullscreen, names, net, ww, hh, c, labelpaths,_datacfg,5);
        jstring info = stoJstring(env,result);
        (env)->ReleaseByteArrayElements(bytes,bytess,0);
        return info;
}
#endif

/*
 * Class:     runtime_detection_Detector
 * Method:    freeNetWork
 * Signature: (J[BLjava/lang/String;FFIIII)[Loperator/detection/yolo/freeNetWork;
 */
JNIEXPORT void JNICALL Java_runtime_detection_Detector_freeNetWork
  (JNIEnv *env, jobject obj, jlong peer) {
	network* net = (network*)peer;
	free_network(net);
  }